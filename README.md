# dssd-network

This is the decentralized network library for the distributed spectral database project.

## Getting started

### Prerequisites
- Instal Rust using [rustup](https://www.rust-lang.org/en-US/install.html)
- Install VirtualBox
- Install Vagrant
- Setup the swarm host
  - $ vagrant box add ubuntu/xenial64
  - $ vagrant up

### Compiling
- Install both build-essential and gcc-multilib (Linux)
  - $ sudo pacman -S build-devel gcc-multilib
  - $ sudo apt-get install build-essential gcc-multilib
- $ cargo build

### Running a node
- $ cargo run [options]

## Diagnostics / Network simulation
- Bootstrap the network using the network diagnostic tool
  - $ dssd-diag test
    - This command will concurrently run all scenarios
  - $ dssd-diag test "scenario-name"
    - This command will bootstrap a network and run the given scenario
  - $ dssd-diag launch-interactive-node 10
    - This command will bootstrap a network with ten nodes and launch an interactive terminal connected to the dssd
