#!/usr/bin/env bash

add-apt-repository ppa:ubuntu-lxc/lxd-git-master
apt-get update

# musl-tools is required to compile using musl
# musl is required to create statically linked rust executables
apt-get install --yes build-essential gcc-multilib musl-tools
apt-get install --yes --only-upgrade lxd

echo "Setting the swarm host..."
newgrp lxd
lxc config set core.https_address :8443

echo "Configuring network..."
lxc network create swarm0
lxc network set swarm0 ipv4.address 10.0.5.1/24
lxc network set swarm0 ipv4.dhcp true
lxc network set swarm0 ipv4.dhcp.ranges 10.0.5.5-10.0.5.254
lxc network set swarm0 ipv4.nat true
lxc network set swarm0 ipv4.routing true
lxc network set swarm0 ipv4.nat true
lxc network set dns.domain swarm
lxc network attach-profile swarm0 default eth0

echo "Configuring storage..."
lxc storage create pool1 dir
lxc profile device add default root disk path=/ pool=pool1
lxc profile device add default sdb disk source=/vagrant/target path=dssd-bin
